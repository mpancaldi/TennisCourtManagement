
class Play {
    private String player,opponent, day, hour;
    private int week;
    private boolean heat, light;

    Play(int week, String day, String hour, String player, String opponent, boolean heat, boolean light) {
        this.week = week;
        this.day = day;
        this.hour = hour;
        this.player = player;
        this.opponent = opponent;
        this.heat = heat;
        this.light = light;
    }

    int getWeek() {
        return week;
    }

    String getDay() {
        return day;
    }

    String getHour() {
        return hour;
    }

    String getPlayer() {
        return player;
    }

    String getOpponent() {
        return opponent;
    }

    boolean getHeat() {
        return heat;
    }

    boolean getLight() {
        return light;
    }
}
