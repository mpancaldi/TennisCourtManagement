
/*
 * Internet and Mobile Services 2016/2017 - Group 9
 *
 * @author Marta Pancaldi, Werner Sperandio, Giulia Burgio
 */

import java.net.*;
import java.io.*;
import java.util.*;

public class readWebPage {
    public static void main(String[] args) throws IOException {

        URL url = new URL("https://next.unibz.it/en");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String inputString;
            while ((inputString = br.readLine()) != null)
                System.out.println(inputString);
        } catch (Exception e) {
            System.out.println(e);
        }

        //instantiate a new URL object by just passing the URL string to the constructor
        URL myURL = new URL("https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#HTTP_session");

        //create a hashmap for storing the various parts of the URL
        HashMap<String, String> map = new HashMap<>();

        //extract the URL parts with the get methods of the URL class and put them into the hashmap
        map.put("protocol", myURL.getProtocol());
        map.put("authority" , myURL.getAuthority());
        map.put("host", myURL.getHost());
        map.put("port" , String.valueOf(myURL.getPort()));
        map.put("path" , myURL.getPath());
        map.put("query" , myURL.getQuery());
        map.put("filename" , myURL.getFile());
        map.put("ref" , myURL.getRef());

        //finally print on the console the hashmap contents
        System.out.println(map);
        /*should return:
        {
            path=/wiki/Hypertext_Transfer_Protocol, 
            protocol=https, 
            ref=HTTP_session, 
            filename=/wiki/Hypertext_Transfer_Protocol, 
            port=-1, 
            authority=en.wikipedia.org, 
            query=null, 
            host=en.wikipedia.org
        }*/
    }
}