
import java.awt.event.*;
import javax.swing.*;

class MenuBuilder {

    MenuBuilder(JFrame frame) {
        JMenu menu = new JMenu("Menu");
        JMenuItem exitMenu = new JMenuItem("Exit");
        exitMenu.addActionListener(new MenuActionListener());
        JMenuItem aboutMenu = new JMenuItem("About");
        aboutMenu.addActionListener(new AboutListener());
        menu.add(exitMenu);
        menu.add(aboutMenu);
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
    }

    private class MenuActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            JMenuItem source = (JMenuItem) event.getSource();
            String text = source.getText();
            if (text.equals("Exit")) {
                System.exit(0);
            }
        }
    }

    private class AboutListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            JOptionPane.showMessageDialog(null, "<html>Some interesting facts:<br><br>1. " +
                    "This program has 10 classes, 48 methods, 726 total lines <br>of code - if I didn't miss some of them. My record;<br>"
                    + "2. I wrote this program in 5 days, 12 hours, 27 minutes<br> and 49 seconds... more or less;<br>"
                    + "3. Before writing this program I didn't like tennis.<br>Now I hate it.<br>"
                    + "4. Don't worry, I was just kidding :) <br>"
                    + "5. One of this sentences is false. Or maybe not only one.<br>"
                    + "6. This message will self destruct in 10, 9, 8...</html>");
        }
    }
}
