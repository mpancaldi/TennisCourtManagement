
import java.io.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.event.*;

public class MainPanel extends JPanel {
    private TabCourtsPanel tabCourts;
    private TabResPanel tabRes;
    private Tab$Panel tab$;
    private JTabbedPane  pane;
    private Scanner scan;

    public MainPanel() throws IOException {
        this.tabCourts = new TabCourtsPanel();
        this.tabRes = new TabResPanel();
        this.tab$ = new Tab$Panel();
        pane = new JTabbedPane();
        pane.addTab("Calendar", tabCourts);
        pane.addTab("Reservation", tabRes);
        pane.addTab("Add Credit", tab$);
        pane.addChangeListener(new tabListener());
        add(pane);
    }

    private class tabListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            String PIN;
            ArrayList<Player> playerList = new ArrayList<>();
            String[] playerData;

            try {
                scan = new Scanner (new File("src/players.dat"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
            }

            while (scan.hasNextLine()) { //creates 10 Player instances from file data
                playerData = scan.nextLine().split(";");
                Player player1 = new Player(playerData[0], playerData[1], Double.parseDouble(playerData[2]));
                playerList.add(player1);
            }

            String[] playerPins = new String[playerList.size()];

            for (int i = 0; i < playerPins.length; i++)
                playerPins[i] = playerList.get(i).getPIN();

            if (pane.getSelectedIndex() == 0)
                try {
                    tabRes.reloadIntoMainPanel();
                    tabCourts.reloadCourtPanel();
                    tab$.reloadIntoMainPanel();

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
                }

            if ((pane.getSelectedIndex() == 1) || (pane.getSelectedIndex() == 2) ) //Asks to insert the PIN to access the reservation tabs

                do {
                    tab$.reloadIntoMainPanel();
                    tabRes.reloadIntoMainPanel();

                    PIN = JOptionPane.showInputDialog(null, "Please enter your PIN: ");
                    if (Arrays.asList(playerPins).contains(PIN)) {
                        JOptionPane.showMessageDialog(null, "PIN was correct!");

                        int nameIndex = Arrays.asList(playerPins).indexOf(PIN); //gets the player data from his PIN
                        String playerName = playerList.get(nameIndex).getName();
                        double balance = playerList.get(nameIndex).getBalance();
                        tabRes.setName(playerName); //and sets the data in the two tabs
                        tabRes.setIndex(nameIndex);
                        tab$.setName(playerName);
                        tab$.setBalance(balance);
                        tab$.setIndex(nameIndex);
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Incorrect PIN. Please try again.");
                    }
                }
                while (!Arrays.asList(playerPins).contains(PIN));
        }
    }
}
