
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class CourtAPanel extends JPanel {
    private JButton nextWeek, lastWeek;
    private JLabel changeWeek;
    private int currentYear, currentWeek;
    private Cal[] calList;

    public CourtAPanel(String path) throws FileNotFoundException{
        loadCalIntoPanel(path);
    }

    private void loadCalIntoPanel(String path) throws FileNotFoundException {
        setPreferredSize (new Dimension (1300,650));

        calList = new Cal[53];
        for (int i = 0; i < calList.length; i++) {
            calList[i] = new Cal();
        }

        Scanner scan = new Scanner(new File(path)); //scans reservation values from corresponding file (A or B)
        ArrayList<Play> dataList = new ArrayList<>();
        String[] data;

        while (scan.hasNextLine()) {
            data = scan.nextLine().split(";"); //scans line by line
            //splits data in arrays, which are used to create an instance of Play
            Play play1 = new Play(Integer.parseInt(data[0]), data[1], data[2], data[3], data[4], //splits data in arrays, which are used to create an instance of Play
                    Boolean.parseBoolean(data[5]), Boolean.parseBoolean(data[6]));
            dataList.add(play1);} //... and adds new Play to ArrayList

        for (Play play : dataList) { //setReservation in correspondent week panel (from 1 to 52)
            calList[play.getWeek()].setReservation(play.getDay(), play.getHour(),
                    play.getPlayer(), play.getOpponent(),
                    play.getHeat(), play.getLight());
        }

        nextWeek = new JButton(">>");
        nextWeek.addActionListener(new weekListener());
        lastWeek = new JButton("<<");
        lastWeek.addActionListener(new weekListener());

        GregorianCalendar cal = new GregorianCalendar();
        currentYear = cal.get(GregorianCalendar.YEAR);
        currentWeek = cal.get(GregorianCalendar.WEEK_OF_YEAR);
        changeWeek = new JLabel("<html><font size ='6'>" + currentYear + ", Week " + currentWeek+ "</font></html>");

        add(lastWeek);
        add(changeWeek);
        add(nextWeek);
        add(calList[currentWeek]);
    }

    void reload(String path) throws FileNotFoundException {
        this.removeAll();
        this.loadCalIntoPanel(path);
    }

    public class weekListener implements ActionListener { //change week number and loads the corresponding week panel
        public void actionPerformed (ActionEvent event){
            if (event.getSource() == nextWeek && currentWeek < 52) {
                currentWeek+=1;
            }
            if (event.getSource() == lastWeek && currentWeek > 1) {
                currentWeek-=1;
            }

            changeWeek.setText("<html><font size ='6'>" + currentYear + ", Week " + currentWeek+ "</font></html>");
            removeAll();
            add(lastWeek);
            add(changeWeek);
            add(nextWeek);
            add(calList[currentWeek]);
        }
    }
}
