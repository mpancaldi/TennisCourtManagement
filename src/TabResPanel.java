
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;

public class TabResPanel extends JPanel {
    private JLabel pinLabel, costLabel;
    private JTextField  oppField;
    private JRadioButton aButton, bButton;
    private JCheckBox heat, light;
    private JComboBox<String> week;
    private JComboBox<String> day;
    private JComboBox<String> hour;
    private String oppName  = "opponent", dataString, name, wdh;
    private int weekVal, dayVal, hourVal, ab = 0, index;
    private double total = 10;
    private boolean heatSel, lightSel;
    private NumberFormat formatter = new DecimalFormat("#0.00");

    public TabResPanel() {
        loadIntoMainPanel();
    }

    private void loadIntoMainPanel() {
        JPanel inPanel = new JPanel();
        inPanel.setPreferredSize(new Dimension(500,650));
        inPanel.setBackground(Color.white);
        inPanel.setBorder(BorderFactory.createEtchedBorder(Color.LIGHT_GRAY, null));

        JPanel upPanel = new JPanel();
        pinLabel = new JLabel("<html><font size = '6'>Welcome, guest!</font></html>");
        upPanel.setPreferredSize(new Dimension(450,50));
        upPanel.setBackground(Color.white);
        upPanel.add(pinLabel);

        JPanel radioPanel = new JPanel();
        radioPanel.setPreferredSize(new Dimension(150,100));
        radioPanel.setBackground(Color.white);
        aButton = new JRadioButton("Court A");
        bButton = new JRadioButton("Court B");
        aButton.addItemListener(new radioListener());
        bButton.addItemListener(new radioListener());

        ButtonGroup abGroup = new ButtonGroup();
        abGroup.add(aButton);
        abGroup.add(bButton);
        radioPanel.add(aButton);
        radioPanel.add(bButton);

        JPanel oppPanel = new JPanel();
        JLabel oppLabel = new JLabel("Enter your opponent's name: ");
        oppField = new JTextField(10);
        oppPanel.setPreferredSize(new Dimension(450,50));
        oppPanel.setBackground(Color.white);
        oppPanel.add(oppLabel);
        oppPanel.add(oppField);

        JPanel checkPanel = new JPanel();
        checkPanel.setPreferredSize(new Dimension(150,100));
        checkPanel.setBackground(Color.white);
        light = new JCheckBox("Light (+ €2.00)");
        light.addItemListener(new radioListener());
        heat = new JCheckBox ("Heating (+ €3.00)");
        heat.addItemListener(new radioListener());
        checkPanel.add(heat);
        checkPanel.add(light);

        GregorianCalendar cal = new GregorianCalendar();
        JPanel weekPanel = new JPanel();
        weekPanel.setPreferredSize(new Dimension(400,80));
        weekPanel.setBackground(Color.white);
        JLabel chWeek = new JLabel("Choose week:");  //starts the count from the current week => previous weeks are not bookable
        int weekNum = cal.get(GregorianCalendar.WEEK_OF_YEAR);
        String[] weekString = new String[52];
        for (int i=(weekNum-1); i<weekString.length;i++){
            weekString[i] = weekNum + "";
            weekNum++;
        }
        week = new JComboBox<>(weekString);
        week.addItemListener(new radioListener());
        weekPanel.add(chWeek);
        weekPanel.add(week);

        JPanel dayPanel = new JPanel();
        dayPanel.setPreferredSize(new Dimension(400,80));
        dayPanel.setBackground(Color.white);
        JLabel chDay = new JLabel("Choose day:");
        String[] dayString = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        day = new JComboBox<>(dayString);
        day.addItemListener(new radioListener());
        dayPanel.add(chDay);
        dayPanel.add(day);

        JPanel timePanel = new JPanel();
        timePanel.setPreferredSize(new Dimension(400,60));
        timePanel.setBackground(Color.white);
        JLabel chTime = new JLabel("Choose time");
        String[] hourString = new String[14];
        int hourNum = 8;
        for (int i=0; i<14; i++) {
            hourString[i]= hourNum + ".00 - " + (hourNum+1) + ".00";
            hourNum++;
        }
        hour = new JComboBox<>(hourString);
        hour.addItemListener(new radioListener());
        timePanel.add(chTime);
        timePanel.add(hour);

        JButton reserve = new JButton("<html><font size = '5'>Reserve</font></html>");
        reserve.setPreferredSize(new Dimension(130,50));
        reserve.addActionListener(new reserveListener());
        JPanel reservePanel = new JPanel();
        reservePanel.setPreferredSize(new Dimension(200,70));
        reservePanel.setBackground(Color.white);
        reservePanel.add(reserve);

        costLabel = new JLabel("<html><font size ='5'><br>Total = € " + formatter.format(total) +"</font></html>");
        JPanel costPanel = new JPanel();
        costPanel.setPreferredSize(new Dimension(370,80));
        costPanel.setBackground(Color.LIGHT_GRAY);
        costPanel.setBorder(BorderFactory.createEtchedBorder(Color.DARK_GRAY,null));
        costPanel.add(costLabel);

        inPanel.add(upPanel);
        inPanel.add(oppPanel);
        inPanel.add(radioPanel);
        inPanel.add(checkPanel);
        inPanel.add(weekPanel);
        inPanel.add(dayPanel);
        inPanel.add(timePanel);
        inPanel.add(reservePanel);
        inPanel.add(costPanel);
        add(inPanel);
    }

    void setIndex(int loadIndex) {index = loadIndex;}

    private boolean checkBalance() throws IOException {
        ArrayList<Player> playerList = new ArrayList<>(); //creates an arraylist of players from file data,
        String[] playerData;
        Scanner scan = new Scanner (new File("src/players.dat"));

        while (scan.hasNextLine()) {
            playerData = scan.nextLine().split(";");
            Player player1 = new Player(playerData[0], playerData[1], Double.parseDouble(playerData[2]));
            playerList.add(player1);
        }

        double balance = playerList.get(index).getBalance();
        PrintWriter playerWriter;

        if ((balance - total) >= 0) { //checks the balance of the current player,
            playerList.get(index).setBalance(balance - total); //and pays if there is enough credit.
            playerWriter = new PrintWriter(new BufferedWriter(new FileWriter("src/players.dat", false)));
            for (Player player : playerList) { //Then the file is overwritten with new data
                playerWriter.println(player.toString());
            }
            playerWriter.close();
            return true;

        } else { //Else it asks to add credit
            JOptionPane.showMessageDialog(null, "<html>Not enough credit on your account. <br>Please recharge it.</html>");
            playerWriter = new PrintWriter(new BufferedWriter(new FileWriter("src/players.dat", false)));
            for (Player player : playerList) {
                playerWriter.println(player.toString());
            }
            playerWriter.close();
            return false;
        }
    }

    private void checkReservation(String path) throws IOException {
        Scanner scanner = new Scanner(new File(path));
        ArrayList<Play> playData = new ArrayList<>();

        while (scanner.hasNextLine()) { //creates arraylist from file A or B
            String[] data = scanner.nextLine().split(";");
            Play play2 = new Play(Integer.parseInt(data[0]), data[1], data[2], data[3], data[4],
                    Boolean.parseBoolean(data[5]), Boolean.parseBoolean(data[6]));
            playData.add(play2);
        }

        ArrayList<String> wdhList = new ArrayList<String>();

        for (Play playDatum : playData) {
            String wdhString = playDatum.getWeek() + ";" + playDatum.getDay() + ";" + playDatum.getHour();
            wdhList.add(wdhString); //creates new arraylist only with data to check (week, day and hour numbers)
        }

        if (checkBalance()) {
            if(!wdhList.contains(wdh)){ //checks if there exists already a reservation with same week, day and hour value                                         
                PrintWriter resWriter = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
                resWriter.println(dataString); //If yes, it saves the new reservation to file
                resWriter.close();
                JOptionPane.showMessageDialog(null,"<html>Congratulations!<br>Your reservation was successful.</html>");
            } else {
                JOptionPane.showMessageDialog(null,"<html>The combination you chose was already booked."
                        + "<br>Please check the calendar and select another one.</html>");
            }
        }
    }

    void reloadIntoMainPanel() {
        this.removeAll();
        this.loadIntoMainPanel();
    }

    public void setName(String loadName){
        name = loadName;
        pinLabel.setText("<html><font size = '6'>Welcome, " + name + "!</font></html>");
    }

    public class reserveListener implements ActionListener {
        public void actionPerformed (ActionEvent event) {
            dataString = weekVal + ";" + dayVal + ";" + hourVal + ";" + name + ";" + oppName + ";" + heatSel + ";" + lightSel + ";";
            wdh = weekVal+ ";"+ dayVal+ ";" +hourVal;

            try {
                String APath = "src/reservationA.dat";
                if (ab == 1) {
                    checkReservation(APath);
                }
                String BPath = "src/reservationB.dat";
                if (ab == 2) {
                    checkReservation(BPath);
                }
                heat.setSelected(false);
                light.setSelected(false);

            } catch (IOException ex) {
                Logger.getLogger(TabResPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public class radioListener implements ItemListener {
        public void itemStateChanged(ItemEvent event) {
            if (aButton.isSelected()) ab = 1;
            else if (bButton.isSelected()) ab=2;

            lightSel = light.isSelected();
            heatSel = heat.isSelected();

            if (light.isSelected() && heat.isSelected()) total = 15;
            if (light.isSelected() && !heat.isSelected()) total = 12;
            if (!light.isSelected() && heat.isSelected()) total = 13;
            if (!light.isSelected() && !heat.isSelected()) total = 10;

            costLabel.setText("<html><font size ='5'><br>Total = € " + formatter.format(total) +"</font></html>");
            oppName = oppField.getText();
            weekVal = week.getSelectedIndex() + 1;
            dayVal = day.getSelectedIndex() + 1;
            hourVal = hour.getSelectedIndex();
        }
    }
}