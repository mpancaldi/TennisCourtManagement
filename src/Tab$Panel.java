
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;

public class Tab$Panel extends JPanel {
    private JLabel id;
    private JLabel balance;
    private JTextField cardField, creditField;
    private int index;
    private double balanceNum = 0;
    private NumberFormat formatter = new DecimalFormat("#0.00");
    private Scanner scan;
    private PrintWriter playerWriter;

    public Tab$Panel() {
        loadIntoMainPanel();
    }

    void setIndex(int loadIndex) {
        index = loadIndex;
    }

    void setBalance(double loadBalance){
        balanceNum = loadBalance;
        balance.setText("<html><font size = '5'>Current balance: € " + formatter.format(balanceNum) +"</font></html>");
    }

    private void loadIntoMainPanel() {
        JPanel inPanel = new JPanel();
        inPanel.setPreferredSize(new Dimension(500,650));
        inPanel.setBackground(Color.white);
        inPanel.setBorder(BorderFactory.createEtchedBorder(Color.LIGHT_GRAY, null));

        JPanel idPanel = new JPanel();
        idPanel.setPreferredSize(new Dimension(360,80));
        idPanel.setBackground(Color.white);
        balanceNum = 0;

        id = new JLabel("<html><font size = '6'>Welcome, guest!</font></html>");
        balance = new JLabel("<html><font size = '5'>Current balance: € " + formatter.format(balanceNum) +"</font></html>");
        idPanel.add(id);
        idPanel.add(balance);

        JLabel cardLabel = new JLabel("Enter your credit card number: ");
        cardField = new JTextField(11);
        JPanel cardPanel = new JPanel();
        cardPanel.setPreferredSize(new Dimension(300,80));
        cardPanel.setBackground(Color.white);
        cardPanel.add(cardLabel);
        cardPanel.add(cardField);

        JLabel creditLabel = new JLabel("Enter the credit you wish to add: €");
        creditField = new JTextField(3);
        creditField.setText("0.00");
        JPanel creditPanel = new JPanel();
        creditPanel.setPreferredSize(new Dimension(400,80));
        creditPanel.setBackground(Color.white);
        creditPanel.add(creditLabel);
        creditPanel.add(creditField);

        JButton addCredit = new JButton("<html><font size = '5'>Add Credit</font></html>");
        addCredit.setPreferredSize(new Dimension(150,50));
        addCredit.addActionListener(new addCreditListener());

        JPanel upPanel = new JPanel();
        upPanel.setPreferredSize(new Dimension(400,70));
        upPanel.setBackground(Color.white);

        inPanel.add(upPanel);
        inPanel.add(idPanel);
        inPanel.add(cardPanel);
        inPanel.add(creditPanel);
        inPanel.add(addCredit);
        add(inPanel);
    }

    public void setName(String loadName){
        id.setText("<html><font size = '6'>Welcome, " + loadName + "!</font></html>");
    }

    void reloadIntoMainPanel() {
        this.removeAll();
        this.loadIntoMainPanel();
    }

    public class addCreditListener implements ActionListener {
        public void actionPerformed (ActionEvent event) {
            ArrayList<Player> playerList = new ArrayList<>();
            String[] playerData;

            try {
                scan = new Scanner (new File("src/players.dat"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Tab$Panel.class.getName()).log(Level.SEVERE, null, ex);
            }

            while (scan.hasNextLine()) {
                playerData = scan.nextLine().split(";");
                Player player1 = new Player(playerData[0], playerData[1], Double.parseDouble(playerData[2]));
                playerList.add(player1);
            }

            try {
                double credit = Double.parseDouble(creditField.getText());
                //50:50 - simulates a possible error in the payment process
                Random random = new Random();
                int num = random.nextInt(10);
                if (num % 2 == 0) {
                    balanceNum += credit;
                    playerList.get(index).setBalance(balanceNum);

                    try {
                        playerWriter = new PrintWriter(new BufferedWriter(
                                new FileWriter("src/players.dat", false)));
                    } catch (IOException ex) {
                        Logger.getLogger(Tab$Panel.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    for (Player player : playerList) {
                        playerWriter.println(player.toString());
                    }

                    playerWriter.close();

                    balance.setText("<html><font size = '5'>Current balance: € " + formatter.format(balanceNum) +"</font></html>");
                    JOptionPane.showMessageDialog(null, "<html>Payment was ok!</html>");
                    creditField.setText("0.00");
                }
                else {
                    JOptionPane.showMessageDialog(null,
                            "<html>Ooops... something went wrong with the payment.<br>Please retry.</html>");
                }
            }
            catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(null, "Please enter numeric values.");
                cardField.setText("");
                creditField.setText("0.00");
            }
        }
    }
}
