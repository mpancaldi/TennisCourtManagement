
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Cal extends JPanel {
    private DefaultTableModel tableModel;

    // Draws a 8x14 table. Reservation values are read from file and set into the correspondent cell
    public Cal() {
        setPreferredSize (new Dimension(1300,650));
        tableModel = new DefaultTableModel();

        JTable tableCalendar = new JTable(tableModel) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        JScrollPane scrollPane = new JScrollPane(tableCalendar);

        tableCalendar.setRowHeight(90);
        tableCalendar.setGridColor(Color.gray);
        tableModel.setRowCount(14);
        String[] headers = {"Hours", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"};

        for (int i = 0; i < 8; i++){
            tableModel.addColumn(headers[i]);
        }

        int hour = 8;
        for (int i = 0; i < 14; i++) {
            tableModel.setValueAt(hour + ".00 - " + (hour + 1) +".00", i, 0);
            hour +=1;
        }

        scrollPane.setPreferredSize(new Dimension(1250,600));
        add(scrollPane);
    }

    void setReservation(String day, String hour, String player, String opponent, boolean heat, boolean light) {
        String heatStr, lightStr;
        heatStr = heat ? "yes" : "no";
        lightStr = light ? "yes" : "no";
        tableModel.setValueAt("<html><font color='blue'>PLAYER: " + player + "<br>OPPONENT: "
                + opponent + "<br>HEATING: " + heatStr + "<br>LIGHT: " + lightStr + "</font></html>",
                Integer.parseInt(hour), Integer.parseInt(day));
    }
}
