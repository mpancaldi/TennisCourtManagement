
import javax.swing.JFrame;
import java.io.IOException;

public class TennisCourtsManager {
    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        new MenuBuilder(frame);
        MainPanel mainPanel = new MainPanel();
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setVisible(true);
    }
}
