
public class Player {
    private String name, PIN;
    private double balance;

    Player(String name, String PIN, double balance){
        this.name = name;
        this.PIN = PIN;
        this.balance = balance;
    }

    String getName() {
        return name;
    }

    String getPIN() {
        return PIN;
    }

    double getBalance() {
        return balance;
    }

    void setBalance(double newBalance) {
        balance = newBalance;
    }

    public String toString() {
        return name + ";" + PIN + ";" + balance + ";";
    }
}
