
import java.io.*;
import javax.swing.*;

public class TabCourtsPanel extends JPanel{
    private CourtAPanel APanel;
    private CourtAPanel BPanel;
    private String APath = "src/reservationA.dat", BPath = "src/reservationB.dat";

    public TabCourtsPanel() throws FileNotFoundException {
        JTabbedPane ABTab = new JTabbedPane();
        APanel = new CourtAPanel(APath);
        BPanel = new CourtAPanel(BPath);
        ABTab.add("A Court", APanel);
        ABTab.add("B Court", BPanel);
        add(ABTab);
    }

    void reloadCourtPanel() throws FileNotFoundException {
        APanel.reload(APath);
        BPanel.reload(BPath);
    }
}